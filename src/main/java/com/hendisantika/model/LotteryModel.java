package com.hendisantika.model;

public class LotteryModel {
	private int topThree;
	private int topThreePrice;
	
	private int tod;
	private int todPrice;
	
	private int topTwo;
	private int topTwoPrice;
	
	private int underTwo;
	private int underTwoPrice;
	
	private int run;
	private int runPrice;
	
	private String sumTopThreePrice;
	private String sumTodPrice;
	private String sumTopTwoPrice;
	private String sumUnderTwoPrice;
	
	private String startDate;
	private String endDate;
	
	private String todStr;
	
	public String getTodStr() {
		return todStr;
	}
	public void setTodStr(String todStr) {
		this.todStr = todStr;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getSumTopThreePrice() {
		return sumTopThreePrice;
	}
	public void setSumTopThreePrice(String sumTopThreePrice) {
		this.sumTopThreePrice = sumTopThreePrice;
	}
	public String getSumTodPrice() {
		return sumTodPrice;
	}
	public void setSumTodPrice(String sumTodPrice) {
		this.sumTodPrice = sumTodPrice;
	}
	public String getSumTopTwoPrice() {
		return sumTopTwoPrice;
	}
	public void setSumTopTwoPrice(String sumTopTwoPrice) {
		this.sumTopTwoPrice = sumTopTwoPrice;
	}
	public String getSumUnderTwoPrice() {
		return sumUnderTwoPrice;
	}
	public void setSumUnderTwoPrice(String sumUnderTwoPrice) {
		this.sumUnderTwoPrice = sumUnderTwoPrice;
	}
	public int getTopThree() {
		return topThree;
	}
	public void setTopThree(int topThree) {
		this.topThree = topThree;
	}
	public int getTod() {
		return tod;
	}
	public void setTod(int tod) {
		this.tod = tod;
	}
	public int getTopTwo() {
		return topTwo;
	}
	public void setTopTwo(int topTwo) {
		this.topTwo = topTwo;
	}
	public int getUnderTwo() {
		return underTwo;
	}
	public void setUnderTwo(int underTwo) {
		this.underTwo = underTwo;
	}
	public int getTopThreePrice() {
		return topThreePrice;
	}
	public void setTopThreePrice(int topThreePrice) {
		this.topThreePrice = topThreePrice;
	}
	public int getTodPrice() {
		return todPrice;
	}
	public void setTodPrice(int todPrice) {
		this.todPrice = todPrice;
	}
	public int getTopTwoPrice() {
		return topTwoPrice;
	}
	public void setTopTwoPrice(int topTwoPrice) {
		this.topTwoPrice = topTwoPrice;
	}
	public int getUnderTwoPrice() {
		return underTwoPrice;
	}
	public void setUnderTwoPrice(int underTwoPrice) {
		this.underTwoPrice = underTwoPrice;
	}
	public int getRun() {
		return run;
	}
	public void setRun(int run) {
		this.run = run;
	}
	public int getRunPrice() {
		return runPrice;
	}
	public void setRunPrice(int runPrice) {
		this.runPrice = runPrice;
	}
	
}

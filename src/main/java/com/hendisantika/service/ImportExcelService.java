package com.hendisantika.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hendisantika.entity.History;
import com.hendisantika.entity.Lottery;
import com.hendisantika.repository.HistoryRepository;
import com.hendisantika.repository.HistoryRepositoryJPA;
import com.hendisantika.repository.LotteryRepositoryJPA;

@Service
public class ImportExcelService {
	
	
	 @Autowired 
	 private LotteryRepositoryJPA lotteryRepositoryJpa;
	  
	 @Autowired 
	 private HistoryRepositoryJPA historyRepositoryJpa;
	 
	 @Autowired 
	 private HistoryRepository historyRepository;
	 

	public void readFileExcel(MultipartFile files) throws Exception {
		HttpStatus status = HttpStatus.OK;
       List<Lottery> lotteryList = new ArrayList<>();
		  try {
			  XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
		        XSSFSheet worksheet = workbook.getSheetAt(0);
		        History history = new History();
		        
		        List<String> listTopThree = new ArrayList<>();
		        List<String> listTod = new ArrayList<>();
		        List<String> listTopTwo = new ArrayList<>();
		        List<String> listUnderTwo = new ArrayList<>();
		        List<String> listRun = new ArrayList<>();
		        List<String> listUnderRun = new ArrayList<>();
		        
		        Map<String, Integer> mapTopThree = new HashMap<>();
		        Map<String, Integer> mapTod = new HashMap<>();
		        Map<String, Integer> mapTopTwo = new HashMap<>();
		        Map<String, Integer> mapUnderTwo = new HashMap<>();
		        Map<String, Integer> mapRun = new HashMap<>();
		        Map<String, Integer> mapUnderRun = new HashMap<>();
		        
		        int sumTopThree = 0;
		        int sumTod = 0;
		        int sumTopTwo = 0;
		        int sumUnderTwo = 0;
		        int sumRun = 0;
		        int sumUnderRun = 0;
		        
		        for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
		        	XSSFRow row = worksheet.getRow(index);
		        	
		        	// Create by : 
		        	if(index == 0) {
		        		history.setCreateBy(row.getCell(1).getStringCellValue());
		        	}
		        	
		            if (index > 2) {
		            	
		            	Lottery lottery = new Lottery();
		                
		                String topThree = "";
		                int topThreePrice = 0;
		                String tod = "";
		                int todPrice = 0;
		                String topTwo = "";
		                int topTwoPrice = 0;
		                String underTwo = "";
		                int underTwoPrice = 0;
		                String run = "";
		                int runPrice = 0;
		                String underRun = "";
		                int underRunPrice = 0;
		                
		                //TopThree
		                if(row.getCell(0) != null) {
		                	 topThree =(String) row.getCell(0).getStringCellValue();
		                	 if(row.getCell(1) != null) {
		                		 topThreePrice = Integer.valueOf((int) row.getCell(1).getNumericCellValue());
		                		 sumTopThree = sumTopThree + topThreePrice;
		    				 }
		                	 
		                	 if(listTopThree.contains(topThree)) {
		                		int dup = mapTopThree.get(topThree);
		                		mapTopThree.put(topThree, dup+topThreePrice);
		                	 } else {
		                		 listTopThree.add(topThree);
		                		 mapTopThree.put(topThree, topThreePrice);
		                	 }
		                	 
		                }
						
						//Tod
						if(row.getCell(3) != null) {
							tod = (String) row.getCell(3).getStringCellValue();
							if(row.getCell(4) != null) {
								todPrice = Integer.valueOf((int) row.getCell(4).getNumericCellValue());
								sumTod = sumTod + todPrice;
							}
							
							if(listTod.contains(tod)) {
							   int dup = mapTod.get(tod);
							   mapTod.put(tod, dup+todPrice);
							} else {
								listTod.add(tod);
								mapTod.put(tod, todPrice);
							}
							
						}
						
						//TopTwo
						if(row.getCell(6) != null) {
							topTwo = (String) row.getCell(6).getStringCellValue();
							 if(row.getCell(7) != null) {
								 topTwoPrice = Integer.valueOf((int) row.getCell(7).getNumericCellValue());
								 sumTopTwo = sumTopTwo + topTwoPrice;
							}
							 
							if(listTopTwo.contains(topTwo)) {
								int dup = mapTod.get(topTwo);
								mapTopTwo.put(topTwo, dup+topTwoPrice);
							} else {
								listTopTwo.add(topTwo);
								mapTopTwo.put(topTwo, topTwoPrice);
							}
						}
						
						//UnderTwo
						if(row.getCell(9) != null) {
							underTwo = (String) row.getCell(9).getStringCellValue();
							if(row.getCell(10) != null) {
								underTwoPrice = Integer.valueOf((int) row.getCell(10).getNumericCellValue());
								sumUnderTwo = sumUnderTwo + underTwoPrice;
							} 
							
							if(listUnderTwo.contains(underTwo)) {
								int dup = mapTod.get(underTwo);
								mapUnderTwo.put(underTwo, dup+underTwoPrice);
							} else {
								listUnderTwo.add(underTwo);
								mapUnderTwo.put(underTwo, underTwoPrice);
							}
						}
						
						//RunTop
						if(row.getCell(12) != null) {
							run = (String) row.getCell(12).getStringCellValue();
							if(row.getCell(13) != null) {
								runPrice = Integer.valueOf((int) row.getCell(13).getNumericCellValue());
								sumRun = sumRun + runPrice;
							} 
							
							if(listRun.contains(run)) {
								int dup = mapTod.get(run);
								mapRun.put(run, dup+runPrice);
							} else {
								listRun.add(run);
								mapRun.put(run, runPrice);
							}
						}
						
						//Under Run
						if(row.getCell(15) != null) {
							underRun = (String) row.getCell(15).getStringCellValue();
							if(row.getCell(16) != null) {
								underRunPrice = Integer.valueOf((int) row.getCell(16).getNumericCellValue());
								sumUnderRun = sumUnderRun + underRunPrice;
							} 
							
							if(listUnderRun.contains(underRun)) {
								int dup = mapTod.get(underRun);
								mapRun.put(underRun, dup+underRunPrice);
							} else {
								listUnderRun.add(underRun);
								mapUnderRun.put(underRun, underRunPrice);
							}
						}
						
						if(topThreePrice == 0 && todPrice == 0 && topTwoPrice==0 && underTwoPrice == 0 && runPrice == 0 && underRunPrice == 0) {
							break;
						}
						

		                lottery.setTopThree(topThree);
		                lottery.setTopThreePrice(topThreePrice);
		                lottery.setTod(tod);
		                lottery.setTodPrice(todPrice);
		                lottery.setTopTwo(topTwo);
		                lottery.setTopTwoPrice(topTwoPrice);
		                lottery.setUnderTwo(underTwo);
		                lottery.setUnderTwoPrice(underTwoPrice);
		                lottery.setRun(run);
		                lottery.setRunPrice(runPrice);
		                lottery.setUnderRun(underRun);
		                lottery.setUnderRunPrice(underRunPrice);
		                lottery.setCreateDate(new Date());
		                lotteryList.add(lottery);
		                
		            }
		        }
				
		      // select group
		       History findGroup = historyRepository.findGroup();
		       
		      //Save History
		       history.setSumTopThreePrice(sumTopThree);
		       history.setSumTodPrice(sumTod);
		       history.setSumTopTwoPrice(sumTopTwo);
		       history.setSumUnderTwoPrice(sumUnderTwo);
		       history.setSumRunPrice(sumRun);
		       history.setSumUnderRunPrice(sumUnderRun);
		       history.setSumPrice(sumTopThree+sumTod+sumTopTwo+sumUnderTwo+sumRun+sumUnderRun);
		       history.setCreateDate(new Date());
		       history.setSaveFrom("Excel");
		       history.setGroupLottery(findGroup.getGroupLottery()+1);
		       historyRepositoryJpa.save(history); 
		       
		        
		      //Save Lottery
		       for (Lottery lot : lotteryList) {
		    	   lot.setGroupLottery(findGroup.getGroupLottery()+1);
		    	   lotteryRepositoryJpa.save(lot);
		       }
		       
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
       
	}


}

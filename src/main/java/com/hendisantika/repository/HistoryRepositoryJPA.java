package com.hendisantika.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hendisantika.entity.History;

@Repository
public interface HistoryRepositoryJPA extends JpaRepository<History, Long> {

	@Query(value = "SELECT TOP (1) group_lottery FROM history order by group_lottery desc", nativeQuery = true)
	int findGroup();

}
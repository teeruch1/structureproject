package com.hendisantika.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hendisantika.entity.UserLotter;
import com.hendisantika.repository.UserRepositoryJPA;

@Service
public class UserService {

	@Autowired
	private UserRepositoryJPA userRepositoryJPA;

	public List<UserLotter> findAll() {
		return userRepositoryJPA.findAll();
	}
}

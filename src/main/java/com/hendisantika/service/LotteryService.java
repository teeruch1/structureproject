package com.hendisantika.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hendisantika.entity.Lottery;
import com.hendisantika.model.HistoryModel;
import com.hendisantika.repository.HistoryRepositoryJPA;
import com.hendisantika.repository.LotteryRepository;
import com.hendisantika.repository.LotteryRepositoryJPA;

@Service
public class LotteryService {
	
	@Autowired
	private LotteryRepositoryJPA lotteryRepositoryJPA;
	
	@Autowired
	private HistoryRepositoryJPA historyRepositoryJPA;
	
	@Autowired
	private LotteryRepository lotteryRepository;
	
	@Autowired
	private UtilService utilService;

	public List<Lottery> fildAll() throws Exception {
		return lotteryRepositoryJPA.findAll();
	}

	public List<Lottery> getDashBoard(String startDate,String endDate) throws Exception {
		return lotteryRepository.getDashBoard(startDate,endDate);
	}
	
	public List<Lottery> getDashBoardPrice(String startDate,String endDate) throws Exception {
		return lotteryRepository.getDashBoardPrice(startDate,endDate);
	}
	
	public List<Lottery> LotteryTopThree(String startDate, String endDate) throws Exception {
		return lotteryRepository.LotteryTopThree(startDate,endDate);
	}

	public List<Lottery> LotteryTopThreePrice(String startDate, String endDate) throws Exception {
		return lotteryRepository.LotteryTopThreePrice(startDate,endDate);
	}

	public List<Lottery> LotteryTod(String startDate, String endDate, String tod) {

		Object[] obj = new Object[6];
		try {
			if(null != tod && !"".equals(tod)) {
				if(tod.length() == 3) {
					utilService.random6(obj,tod);
				} else {
					tod = "";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	
		return lotteryRepository.LotteryTod(startDate,endDate,tod,obj);
	}

	public List<Lottery> LotteryTodPrice(String startDate, String endDate, String tod) {
		
		Object[] obj = new Object[6];
		try {
			if(null != tod && !"".equals(tod)) {
				if(tod.length() == 3) {
					utilService.random6(obj,tod);
				} else {
					tod = "";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return lotteryRepository.LotteryTodPrice(startDate,endDate,tod,obj);
	}

	public List<Lottery> LotteryTopTwo(String startDate, String endDate) {
		return lotteryRepository.LotteryTopTwo(startDate,endDate);
	}

	public List<Lottery> LotteryTopTwoPrice(String startDate, String endDate) {
		return lotteryRepository.LotteryTopTwoPrice(startDate,endDate);
	}

	public List<Lottery> LotteryUnderTwo(String startDate, String endDate) {
		return lotteryRepository.LotteryUnderTwo(startDate,endDate);
	}

	public List<Lottery> LotteryUnderTwoPrice(String startDate, String endDate) {
		return lotteryRepository.LotteryUnderTwoPrice(startDate,endDate);
	}

	public List<Lottery> LotteryRun(String startDate, String endDate) {
		return lotteryRepository.LotteryRun(startDate,endDate);
	}

	public List<Lottery> LotteryRunPrice(String startDate, String endDate) {
		return lotteryRepository.LotteryRunPrice(startDate,endDate);
	}

	public List<Lottery> LotteryUnderRun(String startDate, String endDate) {
		return lotteryRepository.LotteryUnderRun(startDate,endDate);
	}

	public List<Lottery> LotteryUnderRunPrice(String startDate, String endDate) {
		return lotteryRepository.LotteryUnderRunPrice(startDate,endDate);
	}

	public List<HistoryModel> getHistory(String startDate, String endDate) throws Exception {
		return lotteryRepository.getHistory(startDate,endDate);
	}

	public void deleteLotter(String id) {
		lotteryRepository.deleteLottery(id);
		lotteryRepository.deleteHistory(id);
	}
	
	
	
}


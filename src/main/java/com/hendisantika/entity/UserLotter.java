package com.hendisantika.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "UserLotter")
public class UserLotter implements Serializable{
	
	    private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		
		@Column(name = "name")
		private String name;
		
		@Column(name = "sureName")
		private String sureName;
		
		@Column(name = "userLogin")
		private String userLogin;
		
		@Column(name = "password")
		private String password;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "createDate") 
		private Date createDate;
		 
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "updateDate") 
		private Date updateDate;
		
		public Date getCreateDate() {
			return createDate;
		}

		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}

		public Date getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSureName() {
			return sureName;
		}

		public void setSureName(String sureName) {
			this.sureName = sureName;
		}

		public String getUserLogin() {
			return userLogin;
		}

		public void setUserLogin(String userLogin) {
			this.userLogin = userLogin;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}


}
		

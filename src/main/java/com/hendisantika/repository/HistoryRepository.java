package com.hendisantika.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.hendisantika.entity.History;


@Repository
public class HistoryRepository {
	
	@PersistenceContext
	private EntityManager entityManager;

	public History findGroup() throws Exception {
		StringBuilder sql = new StringBuilder();
		History history = new History();
		int group = 0;
		try {
			sql.append("SELECT TOP (1) group_lottery FROM history order by group_lottery desc");
			
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			List dataList = query.getResultList();
			
			for(Object data : dataList) {
				System.out.println((int) data);
				int obj = (int) data;
				history.setGroupLottery(obj);
			}
			
		} catch (Exception e) {
			throw e;
		}
		return history;
	}
	

}

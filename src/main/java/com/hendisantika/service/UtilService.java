package com.hendisantika.service;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.hendisantika.entity.Lottery;

@Service
public class UtilService {


	public String formatPrice(int sumTopThree) {
		   String number = String.valueOf(sumTopThree);
	       double amount = Double.parseDouble(number);
	       DecimalFormat formatter = new DecimalFormat("###,###,###");
		  return formatter.format(amount);
	}
	
	private String formatPriceDashbord(int sumTopThree) {
		   String number = String.valueOf(sumTopThree);
	       double amount = Double.parseDouble(number);
	       DecimalFormat formatter = new DecimalFormat("###,###,###");
		  return formatter.format(amount);
	}
	
	public void headerDashBoard(ModelAndView modelview, List<Lottery> listDashBoard) {
		for(Lottery obj : listDashBoard) {
			modelview.addObject("sumTopThree", this.formatPrice(obj.getTopThreePrice()));
			modelview.addObject("sumTod", this.formatPrice(obj.getTodPrice()));
			modelview.addObject("sumTopTwo", this.formatPrice(obj.getTopTwoPrice()));
			modelview.addObject("sumUnderTwo", this.formatPrice(obj.getUnderTwoPrice()));
			modelview.addObject("sumRun", this.formatPrice(obj.getRunPrice()));
			modelview.addObject("sumUnderRun", this.formatPrice(obj.getUnderRunPrice()));
			modelview.addObject("sumlottery", this.formatPrice(obj.getTopThreePrice() + obj.getTodPrice() + obj.getTopTwoPrice() + obj.getUnderTwoPrice() + obj.getRunPrice()));
		}
	}

	public void random6(Object[] obj, String tod) {
		String sub1 = "";
		String sub2 = "";
		String sub3 = ""; 
		
		sub1 = tod.substring(0, 1);
		sub2 = tod.substring(1, 2);
		sub3 = tod.substring(2, 3);
		
		obj[0] = tod;
		obj[1] = sub1+sub3+sub2;
		obj[2] = sub2+sub1+sub3;
		obj[3] = sub2+sub3+sub1;
		obj[4] = sub3+sub1+sub2;
		obj[5] = sub3+sub2+sub1;
		
	}



}

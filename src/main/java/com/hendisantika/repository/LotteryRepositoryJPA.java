package com.hendisantika.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hendisantika.entity.Lottery;

public interface LotteryRepositoryJPA  extends JpaRepository<Lottery, Long> {


}

package com.hendisantika.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hendisantika.entity.UserLotter;

public interface UserRepositoryJPA   extends JpaRepository<UserLotter, Long> {

}

package com.hendisantika.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hendisantika.entity.Lottery;
import com.hendisantika.service.ImportExcelService;
import com.hendisantika.service.LotteryService;
import com.hendisantika.service.UtilService;

@Controller
public class ImportExcelController {
	
	@Autowired
	private ImportExcelService importExcelService;
	
	@Autowired
	private UtilService formatService;

	@Autowired
	private LotteryService lotteryService;
	
	private static final String VIEW_DASHBOARD = "dashboard";
	
	private static final SimpleDateFormat sdf_ddMMyyyy = new SimpleDateFormat("dd/MM/yyy",Locale.US);
	
	@PostMapping("/upload")
    public ModelAndView importExcelFile(@RequestParam("file") MultipartFile files) throws IOException {
			ModelAndView modelview = new ModelAndView();
			modelview.setViewName(VIEW_DASHBOARD);
		try {
			System.out.println("Start importExcelFile...");
		    importExcelService.readFileExcel(files);
	        
		 // headerDashBoard
			String startDate = sdf_ddMMyyyy.format(new Date());
			String endDate = sdf_ddMMyyyy.format(new Date());
			List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
			formatService.headerDashBoard(modelview,listDashBoard);
			
			modelview.addObject("startDate",startDate);
			modelview.addObject("endDate",endDate);
	        System.out.println("End importExcelFile...");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        return modelview;
    }

}

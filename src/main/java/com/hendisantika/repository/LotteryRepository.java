package com.hendisantika.repository;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.hendisantika.entity.History;
import com.hendisantika.entity.Lottery;
import com.hendisantika.model.HistoryModel;

@Repository
public class LotteryRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss",Locale.US);
	
	public History findGroup() throws Exception {
		StringBuilder sql = new StringBuilder();
		History history = new History();
		try {
			sql.append("SELECT TOP (1) group_lottery FROM history order by group_lottery desc");
			
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings({ "unchecked", "rawtypes" })
			List dataList = query.getResultList();
			
			for(Object data : dataList) {
				System.out.println((int) data);
				int obj = (int) data;
				history.setGroupLottery(obj);
			}
			
		} catch (Exception e) {
			throw e;
		}
		return history;
	}

	public List<Lottery> getDashBoard(String fromDate, String toDate) throws Exception  {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT SUM(top_three_price) As top_three_price, "
					+ "SUM(tod_price) As tod_price, "
					+"SUM(top_two_price) As top_two_price, "
					+"SUM(under_two_price) As under_two_price, "
					+"SUM(run_price) As run_price, "
					+"SUM(under_run_price) As under_run_price "
					+"FROM lottery WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+fromDate+"' AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+toDate+"'");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				if(data[0] == null) {
					lottery.setTopThreePrice(0);
				} else {
					lottery.setTopThreePrice((int) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTodPrice(0);
				} else {
					lottery.setTodPrice((int) data[1]);
				}
			
				if(data[2] == null) {
					lottery.setTopTwoPrice(0);
				} else {
					lottery.setTopTwoPrice((int) data[2]);
				}
				
				if(data[3] == null) {
					lottery.setUnderTwoPrice(0);
				} else {
					lottery.setUnderTwoPrice((int) data[3]);
				}
				
				if(data[4] == null) {
					lottery.setRunPrice(0);
				} else {
					lottery.setRunPrice((int) data[4]);
				}
				
				if(data[5] == null) {
					lottery.setUnderRunPrice(0);
				} else {
					lottery.setUnderRunPrice((int) data[5]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}
	
	public List<Lottery> getDashBoardPrice(String fromDate, String toDate) throws Exception  {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT SUM(top_three_price) As top_three_price, "
					+ "SUM(tod_price) As tod_price, "
					+"SUM(top_two_price) As top_two_price, "
					+"SUM(under_two_price) As under_two_price, "
					+"SUM(run_price) As run_price, "
					+"SUM(under_run_price) As under_run_price "
					+"FROM lottery WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+fromDate+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+toDate);
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				if(data[0] == null) {
					lottery.setTopThreePrice(0);
				} else {
					lottery.setTopThreePrice((int) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTodPrice(0);
				} else {
					lottery.setTodPrice((int) data[1]);
				}
			
				if(data[2] == null) {
					lottery.setTopTwoPrice(0);
				} else {
					lottery.setTopTwoPrice((int) data[2]);
				}
				
				if(data[3] == null) {
					lottery.setUnderTwoPrice(0);
				} else {
					lottery.setUnderTwoPrice((int) data[3]);
				}
				
				if(data[4] == null) {
					lottery.setRunPrice(0);
				} else {
					lottery.setRunPrice((int) data[4]);
				}
				
				if(data[5] == null) {
					lottery.setUnderRunPrice(0);
				} else {
					lottery.setUnderRunPrice((int) data[5]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTopThree(String startDate, String endDate) throws Exception {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT top_three , SUM(top_three_price) As top_three_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY top_three order by top_three ASC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTopThree((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTopThreePrice(0);
				} else {
					lottery.setTopThreePrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTopThreePrice(String startDate, String endDate) throws Exception  {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT top_three , SUM(top_three_price) As top_three_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY top_three order by top_three_price DESC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTopThree((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTopThreePrice(0);
				} else {
					lottery.setTopThreePrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTod(String startDate, String endDate, String tod, Object[] obj) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			
			if(null != tod && !"".equals(tod)) {
				sql.append("SELECT a.tod, a.tod_price from ( " 
						+"SELECT DISTINCT tod , SUM(tod_price) As tod_price "
						+"FROM lottery "
						+"WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
						+"AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
						+" GROUP BY tod ) As a where tod IN ('"+obj[0]+"','"+obj[1]+"','"+obj[2]+"','"+obj[3]+"','"+obj[4]+"','"+obj[5]+"') order by tod ASC ");
			} else {
				sql.append("SELECT DISTINCT tod , SUM(tod_price) As tod_price "
						+"FROM lottery "
						+"WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
						+"AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
						+"GROUP BY tod order by tod ASC ");
			}
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTod((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTodPrice(0);
				} else {
					lottery.setTodPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTodPrice(String startDate, String endDate, String tod, Object[] obj) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			if(null != tod && !"".equals(tod)) {
				sql.append("SELECT a.tod, a.tod_price from ( " 
						+"SELECT DISTINCT tod , SUM(tod_price) As tod_price "
						+"FROM lottery "
						+"WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
						+"AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
						+" GROUP BY tod ) As a where tod IN ('"+obj[0]+"','"+obj[1]+"','"+obj[2]+"','"+obj[3]+"','"+obj[4]+"','"+obj[5]+"') order by tod_price DESC ");
			} else {
			sql.append("SELECT DISTINCT tod , SUM(tod_price) As tod_price "
					+"FROM lottery "
					+"WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+"AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+"GROUP BY tod order by tod_price DESC ");
			}
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTod((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTodPrice(0);
				} else {
					lottery.setTodPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTopTwo(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT top_two , SUM(top_two_price) As top_two_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY top_two order by top_two ASC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTopTwo((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTopTwoPrice(0);
				} else {
					lottery.setTopTwoPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryTopTwoPrice(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT top_two , SUM(top_two_price) As top_two_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY top_two order by top_two_price DESC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setTopTwo((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setTopTwoPrice(0);
				} else {
					lottery.setTopTwoPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryUnderTwo(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT under_two , SUM(under_two_price) As under_two_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY under_two order by under_two ASC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setUnderTwo((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setUnderTwoPrice(0);
				} else {
					lottery.setUnderTwoPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryUnderTwoPrice(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT under_two , SUM(under_two_price) As under_two_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY under_two order by under_two_price DESC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setUnderTwo((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setUnderTwoPrice(0);
				} else {
					lottery.setUnderTwoPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryRun(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT run , SUM(run_price) As run_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY run order by run ASC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setRun((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setRunPrice(0);
				} else {
					lottery.setRunPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryRunPrice(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT run , SUM(run_price) As run_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY run order by run_price DESC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setRun((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setRunPrice(0);
				} else {
					lottery.setRunPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryUnderRun(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT under_run , SUM(under_run_price) As under_run_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY under_run order by under_run ASC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setUnderRun((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setUnderRunPrice(0);
				} else {
					lottery.setUnderRunPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<Lottery> LotteryUnderRunPrice(String startDate, String endDate) {
		List<Lottery> list = new ArrayList<Lottery>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT DISTINCT under_run , SUM(under_run_price) As under_run_price "
					+" FROM lottery " 
					+" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					+" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'"
					+" GROUP BY under_run order by under_run_price DESC ");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				Lottery lottery = new Lottery();
				String str = (String) data[0];
				if("".equals(str)) {
					continue;
				} else {
					lottery.setUnderRun((String) data[0]);
				}
				
				if(data[1] == null) {
					lottery.setUnderRunPrice(0);
				} else {
					lottery.setUnderRunPrice((int) data[1]);
				}
				
				list.add(lottery);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public List<HistoryModel> getHistory(String startDate, String endDate) throws Exception {
		List<HistoryModel> list = new ArrayList<HistoryModel>();
		StringBuilder sql = new StringBuilder();
		try {
			sql.append("SELECT id "
				      +" ,create_date " 
				      +" ,group_lottery " 
				      +" ,save_from "
				      +" ,sum_price "
				      +" ,sum_run_price "
				      +" ,sum_tod_price "
				      +" ,sum_top_three_price "
				      +" ,sum_top_two_price "
				      +" ,sum_under_run_price "
				      +" ,sum_under_two_price "
				      +" ,update_date "
				      +" FROM history "
				      +" WHERE FORMAT (create_date, 'dd/MM/yyyy') >= '"+startDate+"'"
					 +" AND FORMAT (create_date, 'dd/MM/yyyy') <= '"+endDate+"'");
			
			System.out.println("SQL: "+sql.toString());
			Query query = entityManager.createNativeQuery(sql.toString());
			
			@SuppressWarnings("unchecked")
			List<Object[]> dataList = query.getResultList();
			
			for(Object[] data : dataList) {
				HistoryModel history = new HistoryModel();
				BigInteger bigInt = (BigInteger) data[0];
				history.setId(bigInt.intValue());
				
				Date createDate = (Date) data[1];
				history.setCreateDate(sdf.format(createDate));
				
				history.setGroupLottery((int) data[2]);
				history.setSaveFrom((String) data[3]);
				history.setSumPrice((int) data[4]);
				history.setSumRunPrice((int) data[5]);
				history.setSumTodPrice((int) data[6]);
				history.setSumTopThreePrice((int) data[7]);
				history.setSumTopTwoPrice((int) data[8]);
				history.setSumUnderRunPrice((int) data[9]);
				history.setSumUnderTwoPrice((int) data[10]);
				//history.setUpdateDate((Date) data[10]);
				list.add(history);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return list;
	}

	public void deleteLottery(String id) {
		StringBuilder sql = new StringBuilder();
		sql.append("Delete from lottery where group_lottery = '"+id+"'");
		
		System.out.println("SQL deleteLottery : "+sql.toString());
		Query query = entityManager.createNativeQuery(sql.toString());
		
	}

	public void deleteHistory(String id) {
		StringBuilder sql = new StringBuilder();
		sql.append("Delete from history where group_lottery = '"+id+"'");
		
		System.out.println("SQL deleteHistory : "+sql.toString());
		Query query = entityManager.createNativeQuery(sql.toString());
		
	}

}


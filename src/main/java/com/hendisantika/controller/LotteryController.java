package com.hendisantika.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hendisantika.entity.Lottery;
import com.hendisantika.entity.UserLotter;
import com.hendisantika.model.HistoryModel;
import com.hendisantika.model.LotteryModel;
import com.hendisantika.service.LotteryService;
import com.hendisantika.service.UserService;
import com.hendisantika.service.UtilService;

@Controller
public class LotteryController {

	@Autowired
	private UtilService formatService;

	@Autowired
	private LotteryService lotteryService;
	
	@Autowired
	private UserService userService;
	
	private static final String VIEW_WELCOME = "welcome";
	private static final String VIEW_DASHBOARD = "dashboard";
	private static final String VIEW_TOP_THREE = "lottery/lotteryTopThree";
	private static final String VIEW_TOD = "lottery/lotteryTod";
	private static final String VIEW_TOP_TWO = "lottery/lotteryTopTwo";
	private static final String VIEW_UNDER_TWO = "lottery/lotteryUnderTwo";
	private static final String VIEW_RUN = "lottery/lotteryRun";
	private static final String VIEW_UNDER_RUN = "lottery/lotteryUnderRun";
	private static final String VIEW_UPLOAD_FILE = "uploadFile";
	private static final String VIEW_HISTORY = "lottery/history";
	private static final String VIEW_TEST_PAGE = "testPage";
	
	
	private static final SimpleDateFormat sdf_ddMMyyyy = new SimpleDateFormat("dd/MM/yyy",Locale.US);
	

	@GetMapping("/Lottery")
	public ModelAndView mainWithParam(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model) {
		ModelAndView modelview = new ModelAndView();
		model.addAttribute("message", name);

		modelview.setViewName(VIEW_WELCOME);
		return modelview; // view
	}

	@GetMapping("/LotteryDashBord")
	public ModelAndView LlotteryDashBord(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		model.addAttribute("message", name);
		modelview.setViewName(VIEW_DASHBOARD);
		
		// headerDashBoard
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		this.setSession(session,startDate,endDate);
		
		return modelview; // view
	}

	@GetMapping("/LotteryTopThree")
	public ModelAndView LotteryTopThree(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOP_THREE);
		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		List<Lottery> listTopThree = new ArrayList<>();
		listTopThree = lotteryService.LotteryTopThree(startDate,endDate);
		
		List<Lottery> listTopThreePrice = new ArrayList<>();
		listTopThreePrice = lotteryService.LotteryTopThreePrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("listTopThree", listTopThree);
		modelview.addObject("listTopThreePrice", listTopThreePrice);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryTopThree",method = {RequestMethod.POST})
	public ModelAndView LotteryTopThree(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOP_THREE);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> listTopThree = new ArrayList<>();
		listTopThree = lotteryService.LotteryTopThree(startDate,endDate);
		
		List<Lottery> listTopThreePrice = new ArrayList<>();
		listTopThreePrice = lotteryService.LotteryTopThreePrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("listTopThree", listTopThree);
		modelview.addObject("listTopThreePrice", listTopThreePrice);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}

	@GetMapping("/LotteryTod")
	public ModelAndView LotteryTod(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session)  throws Exception  {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOD);
		
		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		String strTod = (String) session.getAttribute("todStr");
		List<Lottery> listTod = new ArrayList<>();
		listTod = lotteryService.LotteryTod(startDate,endDate,strTod);
		
		List<Lottery> listTodPrice = new ArrayList<>();
		listTodPrice = lotteryService.LotteryTodPrice(startDate,endDate,strTod);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("listTod", listTod);
		modelview.addObject("listTodPrice", listTodPrice);
		modelview.addObject("todStr", strTod == null ? "" : strTod);
		session.setAttribute("todStr", strTod == null ? "" : strTod);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryTod",method = {RequestMethod.POST})
	public ModelAndView LotteryTod(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOD);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> listTod = new ArrayList<>();
		listTod = lotteryService.LotteryTod(startDate,endDate,lottery.getTodStr());
		
		List<Lottery> listTodPrice = new ArrayList<>();
		listTodPrice = lotteryService.LotteryTodPrice(startDate,endDate,lottery.getTodStr());
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("todPrice",lottery.getTodPrice());
		modelview.addObject("listTod", listTod);
		modelview.addObject("listTodPrice", listTodPrice);
		modelview.addObject("todStr", lottery.getTodStr());
		session.setAttribute("todStr", lottery.getTodStr());
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}

	@GetMapping("/LotteryTopTwo")
	public ModelAndView LotteryTopTwo(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOP_TWO);
		
		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		String strTod = (String) session.getAttribute("todStr");
		
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryTopTwo(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryTopTwoPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", strTod == null ? "" : strTod);
		session.setAttribute("todStr", strTod == null ? "" : strTod);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryTopTwo",method = {RequestMethod.POST})
	public ModelAndView LotteryTopTwo(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_TOP_TWO);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryTopTwo(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryTopTwoPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", lottery.getTodStr());
		session.setAttribute("todStr", lottery.getTodStr());
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}

	@GetMapping("/LotteryUnderTwo")
	public ModelAndView LotteryUnderTwo(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_UNDER_TWO);
		
		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		String strTod = (String) session.getAttribute("todStr");
		
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryUnderTwo(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryUnderTwoPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", strTod == null ? "" : strTod);
		session.setAttribute("todStr", strTod == null ? "" : strTod);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryUnderTwo",method = {RequestMethod.POST})
	public ModelAndView LotteryUnderTwo(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_UNDER_TWO);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryUnderTwo(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryUnderTwoPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", lottery.getTodStr());
		session.setAttribute("todStr", lottery.getTodStr());
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}

	@GetMapping("/LotteryRun")
	public ModelAndView LotteryRun(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_RUN);
		

		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		String strTod = (String) session.getAttribute("todStr");
		
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryRun(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryRunPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", strTod == null ? "" : strTod);
		session.setAttribute("todStr", strTod == null ? "" : strTod);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryRun",method = {RequestMethod.POST})
	public ModelAndView LotteryRun(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_RUN);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryRun(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryRunPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", lottery.getTodStr());
		session.setAttribute("todStr", lottery.getTodStr());
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@GetMapping("/LotteryUnderRun")
	public ModelAndView LotteryUnderRun(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_UNDER_RUN);
		

		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		String strTod = (String) session.getAttribute("todStr");
		
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryUnderRun(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryUnderRunPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", strTod == null ? "" : strTod);
		session.setAttribute("todStr", strTod == null ? "" : strTod);
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}
	
	@RequestMapping(value = "/LotteryUnderRun",method = {RequestMethod.POST})
	public ModelAndView LotteryUnderRun(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_UNDER_RUN);
		
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
			startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
		} else {
			startDate =  sdf_ddMMyyyy.format(new Date());
		}
		
		if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
			endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
		} else {
			endDate = sdf_ddMMyyyy.format(new Date());
		}
        
		List<Lottery> list = new ArrayList<>();
		list = lotteryService.LotteryUnderRun(startDate,endDate);
		
		List<Lottery> listPrice = new ArrayList<>();
		listPrice = lotteryService.LotteryUnderRunPrice(startDate,endDate);
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		modelview.addObject("list", list);
		modelview.addObject("listPrice", listPrice);
		modelview.addObject("todStr", lottery.getTodStr());
		session.setAttribute("todStr", lottery.getTodStr());
		
		List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
		formatService.headerDashBoard(modelview,listDashBoard);
		this.setSession(session,startDate,endDate);
		return modelview; // view
	}


	@GetMapping("/UploadFile")
	public ModelAndView UploadFile(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model) {
		ModelAndView modelview = new ModelAndView();
		model.addAttribute("message", name);

		modelview.setViewName(VIEW_UPLOAD_FILE);
		return modelview; // view
	}

	@GetMapping("/TestPage")
	public ModelAndView TestPage(@RequestParam(name = "name", required = false, defaultValue = "") String name,
			Model model) {
		ModelAndView modelview = new ModelAndView();
		model.addAttribute("message", name);
		List<UserLotter> user =  userService.findAll();
		modelview.setViewName(VIEW_TEST_PAGE);
		return modelview; // view
	}
	
	@GetMapping("/{id}")
	public ModelAndView DeleteLottery(@PathVariable(name = "id", required = false) String id,
			Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		System.out.println("id : "+ id);
		
		String startDate = (String) session.getAttribute("startDate");
		String endDate = (String) session.getAttribute("endDate");
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		
		lotteryService.deleteLotter(id);
		this.History(model,session);
		
		modelview.setViewName(VIEW_HISTORY);
		return modelview; // view
	}

	@GetMapping("/History")
	public ModelAndView History(Model model, HttpSession session) throws Exception {
		ModelAndView modelview = new ModelAndView();
		String startDate = sdf_ddMMyyyy.format(new Date());
		String endDate = sdf_ddMMyyyy.format(new Date());
		
		modelview.addObject("startDate",startDate);
		modelview.addObject("endDate",endDate);
		
		List<HistoryModel> history = lotteryService.getHistory(startDate,endDate);
		modelview.addObject("listHistory",history);
		modelview.setViewName(VIEW_HISTORY);
		return modelview; // view
	}
	
	@RequestMapping(value = "/SearchHistory",method = {RequestMethod.POST})
	public ModelAndView SearchHistory(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_HISTORY);
		try {
			String startDate = "";
			String endDate = "";
			if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
				startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
			} else {
				startDate =  sdf_ddMMyyyy.format(new Date());
			}
			
			if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
				endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
			} else {
				endDate = sdf_ddMMyyyy.format(new Date());
			}
			
			// headerDashBoard
			List<HistoryModel> history = lotteryService.getHistory(startDate,endDate);
			modelview.addObject("listHistory",history);
			modelview.addObject("startDate",startDate);
			modelview.addObject("endDate",endDate);
			this.setSession(session,startDate,endDate);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelview; // view
	}
	
	@RequestMapping(value = "/SearchDashBord",method = {RequestMethod.POST})
	public ModelAndView SearchDashBord(LotteryModel lottery, HttpSession session) throws Exception{
		ModelAndView modelview = new ModelAndView();
		modelview.setViewName(VIEW_DASHBOARD);
		try {
			String startDate = "";
			String endDate = "";
			if(lottery.getStartDate() != null || lottery.getStartDate() != "") {
				startDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getStartDate()));
			} else {
				startDate =  sdf_ddMMyyyy.format(new Date());
			}
			
			if(lottery.getEndDate() != null || lottery.getEndDate() != "") {
				endDate = sdf_ddMMyyyy.format(sdf_ddMMyyyy.parse(lottery.getEndDate()));
			} else {
				endDate = sdf_ddMMyyyy.format(new Date());
			}
			
			// headerDashBoard
			List<Lottery> listDashBoard = lotteryService.getDashBoard(startDate,endDate);
			modelview.addObject("startDate",startDate);
			modelview.addObject("endDate",endDate);
			formatService.headerDashBoard(modelview,listDashBoard);
			this.setSession(session,startDate,endDate);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelview; // view
	}
	
	private void setSession(HttpSession session, String startDate, String endDate) {
		session.setAttribute("startDate", startDate);
		session.setAttribute("endDate", endDate);
	}


}

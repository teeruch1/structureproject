package com.hendisantika.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hendisantika.entity.UserLotter;


@Controller
public class WebPageController {
	
	 private static final String LOGIN = "/login";
	 private static final String DASHBOARD = "/dashboard";
	 private static final String FORM = "/form";
	 
	 private static final String VIEW_LOGIN = "lotteryLogin";
	 private static final String VIEW_DASHBOARD = "lotteryDashboard";
	 private static final String VIEW_FORM = "formDashboard";
	 
	
    @GetMapping(LOGIN)
    public ModelAndView login(UserLotter userLottery) {
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName(VIEW_LOGIN);
    	//LotteryLogin
        return modelAndView;
    }
    
	@RequestMapping(value = DASHBOARD,method = {RequestMethod.POST})
    public ModelAndView loginUser(UserLotter userLottery) {
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName(VIEW_DASHBOARD);
        return modelAndView;
    }
    
	
	@RequestMapping(value = FORM,method = {RequestMethod.POST})
    public ModelAndView form(UserLotter userLottery) {
    	ModelAndView modelAndView = new ModelAndView();
    	modelAndView.setViewName(VIEW_FORM);
        return modelAndView;
    }
    
}
